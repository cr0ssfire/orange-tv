package tv.twitch.android.feature.theatre;

import tv.orange.features.ui.UI;
import tv.orange.models.exception.VirtualImpl;

public class LandscapeChatLayoutController {
    /* ... */

    public final void setupForLandscapeDefault() {
        /* ... */

        int value = UI.getLandscapeContainerScale(); // TODO: __INJECT_CODE

        /* ... */

        throw new VirtualImpl();
    }

    /* ... */
}
