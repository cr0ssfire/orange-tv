package tv.orange.core.models.flag

import tv.orange.core.models.flag.Internal.*
import tv.orange.core.models.flag.variants.*
import java.util.*

enum class Flag(
    val preferenceKey: String,
    val titleResName: String?,
    val summaryResName: String?,
    val defaultHolder: ValueHolder,
    var valueHolder: ValueHolder
) {
    DEV_MODE(
        "dev_mode",
        "orange_settings_dev_mode",
        BooleanValue()
    ),
    CHAT_TIMESTAMPS(
        "chat_timestamps",
        "orange_settings_chat_timestamps",
        BooleanValue()
    ),
    DISABLE_LINK_DISCLAIMER(
        "disable_link_disclaimer",
        "orange_settings_disable_link_disclaimer",
        BooleanValue()
    ),
    HIDE_LEADERBOARDS(
        "hide_leaderboards",
        "orange_settings_hide_leaderboards",
        BooleanValue()
    ),
    DISABLE_FAST_BREAD(
        "disable_fast_bread",
        "orange_settings_disable_fast_bread",
        BooleanValue()
    ),
    FOLLOWED_FULL_CARDS(
        "followed_full_cards",
        "orange_settings_followed_full_cards",
        BooleanValue()
    ),
    DISABLE_HYPE_TRAIN(
        "disable_hype_train",
        "orange_settings_disable_hype_train",
        BooleanValue()
    ),
    HIDE_DISCOVER_TAB(
        "hide_discover_tab",
        "orange_settings_hide_discover_tab",
        BooleanValue()
    ),
    VIBRATE_ON_MENTION(
        "vibrate_on_mention",
        "orange_settings_vibrate_on_mention",
        BooleanValue()
    ),
    HIDE_TOP_CHAT_PANEL_VODS(
        "hide_top_chat_panel_vods",
        "orange_settings_hide_top_chat_panel_vods",
        BooleanValue()
    ),
    PRONOUNS(
        "pronouns",
        "orange_settings_pronouns",
        BooleanValue()
    ),
    AUTO_HIDE_MESSAGE_INPUT(
        "auto_hide_message_input",
        "orange_settings_auto_hide_message_input",
        BooleanValue()
    ),
    COMPACT_PLAYER_FOLLOW_VIEW(
        "compact_player_follow_view",
        "orange_settings_compact_player_follow_view",
        BooleanValue()
    ),
    BTTV_EMOTES(
        "bttv_emotes",
        "orange_settings_bttv_emotes",
        BooleanValue(true)
    ),
    FFZ_EMOTES(
        "ffz_emotes",
        "orange_settings_ffz_emotes",
        BooleanValue(true)
    ),
    STV_EMOTES(
        "stv_emotes",
        "orange_settings_stv_emotes",
        BooleanValue(true)
    ),
    ITZ_EMOTES(
        "itz_emotes",
        "orange_settings_itz_emotes",
        BooleanValue(false)
    ),
    FFZ_BADGES(
        "ffz_badges",
        "orange_settings_ffz_badges",
        BooleanValue(true)
    ),
    STV_BADGES(
        "stv_badges",
        "orange_settings_stv_badges",
        BooleanValue(true)
    ),
    CHE_BADGES(
        "che_badges",
        "orange_settings_che_badges",
        BooleanValue(true)
    ),
    CHA_BADGES(
        "cha_badges",
        "orange_settings_cha_badges",
        BooleanValue(true)
    ),
    STV_AVATARS(
        "stv_avatars",
        "orange_settings_stv_avatars",
        BooleanValue(true)
    ),
    CHAT_HISTORY(
        "chat_history",
        "orange_settings_chat_history",
        BooleanValue(true)
    ),
    BRIGHTNESS_GESTURE(
        "brightness_gesture",
        "orange_settings_brightness_gesture",
        BooleanValue(false)
    ),
    VOLUME_GESTURE(
        "volume_gesture",
        "orange_settings_volume_gesture",
        BooleanValue(false)
    ),
    DISABLE_THEATRE_AUTOPLAY(
        "disable_theatre_autoplay",
        "orange_settings_disable_theatre_autoplay",
        BooleanValue(false)
    ),
    DISABLE_STICKY_HEADERS_EP(
        "disable_sticky_headers_ep",
        "orange_settings_disable_sticky_headers_ep",
        BooleanValue()
    ),
    HIDE_BITS_BUTTON(
        "hide_bits_button",
        "orange_settings_hide_bits_button",
        BooleanValue()
    ),
    CLIPFINITY(
        "clipfinity",
        "orange_settings_clipfinity",
        BooleanValue()
    ),
    OKHTTP_LOGGING(
        "okhttp_logging",
        "orange_settings_okhttp_logging",
        BooleanValue()
    ),
    VODHUNTER(
        "vodhunter",
        "orange_settings_vodhunter",
        BooleanValue()
    ),
    BYPASS_CHAT_BAN(
        "bypass_chat_ban",
        "orange_settings_bypass_chat_ban",
        BooleanValue()
    ),
    HIDE_CHAT_HEADER(
        "hide_chat_header",
        "orange_settings_hide_chat_header",
        BooleanValue()
    ),
    HIDE_GAME_SECTION(
        "hide_game_section",
        "orange_settings_hide_game_section",
        BooleanValue()
    ),
    HIDE_RECOMMENDATION_SECTION(
        "hide_recommendation_section",
        "orange_settings_hide_recommendation_section",
        BooleanValue()
    ),
    HIDE_RESUME_WATCHING_SECTION(
        "hide_resume_watching_section",
        "orange_settings_hide_resume_watching_section",
        BooleanValue()
    ),
    HIDE_OFFLINE_CHANNEL_SECTION(
        "hide_offline_channel_section",
        "orange_settings_hide_offline_channel_section",
        BooleanValue()
    ),
    SHOW_TIMER_BUTTON(
        "show_timer_button",
        "orange_settings_show_timer_button",
        BooleanValue(true)
    ),
    SHOW_REFRESH_BUTTON(
        "show_refresh_button",
        "orange_settings_show_refresh_button",
        BooleanValue(true)
    ),
    FORCE_OTA(
        "force_ota",
        "orange_settings_force_ota",
        BooleanValue()
    ),
    SHOW_STATS_BUTTON(
        "show_stats_button",
        "orange_settings_show_stats_button",
        BooleanValue(true)
    ),
    IMPROVED_BACKGROUND_AUDIO(
        "improved_background_audio_v2",
        "orange_settings_improved_background_audio",
        BooleanValue(false)
    ),
    HIDE_UNFOLLOW_BUTTON(
        "hide_unfollow_button",
        "orange_settings_hide_unfollow_button",
        BooleanValue(false)
    ),
    CHAT_SETTINGS(
        "chat_settings",
        "orange_settings_chat_settings",
        BooleanValue(true)
    ),
    HIDE_CREATE_BUTTON(
        "hide_create_button",
        "orange_settings_hide_create_button",
        BooleanValue(false)
    ),
    PLAYER_IMPL(
        "player_impl",
        "orange_settings_player_impl",
        ListValue(PlayerImpl.Default)
    ),
    DELETED_MESSAGES(
        "deleted_messages",
        "orange_settings_deleted_messages",
        ListValue(DeletedMessages.Default),
    ),
    BOTTOM_NAVBAR_POSITION(
        "bottom_navbar_position",
        "orange_settings_bottom_navbar_position",
        ListValue(BottomNavbarPosition.Default),
    ),
    CHAT_FONT_SIZE(
        "chat_font_size",
        "orange_settings_chat_font_size",
        ListValue(FontSize.SP13),
    ),
    EMOTE_QUALITY(
        "emote_quality",
        "orange_settings_emote_quality",
        ListValue(EmoteQuality.MEDIUM),
    ),
    LOCAL_LOGS(
        "local_logs",
        "orange_settings_local_logs",
        ListValue(LocalLogs.L0),
    ),
    UPDATER(
        "updater_final",
        "orange_settings_updater_channel",
        ListValue(UpdateChannel.Release),
    ),
    PINNED_MESSAGE(
        "pinned_message",
        "orange_settings_pinned_message",
        ListValue(PinnedMessageStrategy.Default),
    ),
    Proxy(
        "proxy",
        "orange_settings_proxy",
        ListValue(ProxyImpl.Disabled),
    ),
    LANDSCAPE_CHAT_SIZE(
        "landscape_chat_size",
        "orange_settings_landscape_chat_size",
        IntegerRangeValue(10, 50, 30),
    ),
    FORWARD_SEEK(
        "forward_seek",
        "orange_settings_forward_seek",
        IntegerRangeValue(5, 120, 30, step = 5),
    ),
    REWIND_SEEK(
        "backward_seek",
        "orange_settings_rewind_seek",
        IntegerRangeValue(5, 120, 10, step = 5),
    ),
    MINI_PLAYER_SIZE(
        "mini_player_size",
        "orange_settings_mini_player_size",
        IntegerRangeValue(50, 200, 100, step = 5),
    ),
    VIBRATION_DURATION(
        "vibration_duration",
        "orange_settings_vibration_duration",
        IntegerRangeValue(10, 1000, 100, step = 10),
    ),
    LANDSCAPE_CHAT_OPACITY(
        "landscape_chat_opacity",
        "orange_settings_landscape_chat_opacity",
        IntegerRangeValue(0, 100, 30),
    ),
    EXOPLAYER_VOD_SPEED(
        "exoplayer_vod_speed",
        "orange_settings_exoplayer_vod_speed",
        IntegerRangeValue(25, 200, 100, step = 5),
    ),
    FORCE_EXOPLAYER_FOR_VODS(
        "force_exoplayer_for_vods",
        "orange_settings_force_exoplayer_for_vods",
        BooleanValue(false)
    );

    constructor(prefKey: String, value: ValueHolder) : this(
        preferenceKey = prefKey,
        titleResName = null,
        summaryResName = null,
        defaultHolder = value,
        valueHolder = value
    )

    constructor(prefKey: String, titleResName: String, value: ValueHolder) : this(
        preferenceKey = prefKey,
        titleResName = titleResName,
        summaryResName = titleResName + "_desc",
        defaultHolder = value,
        valueHolder = value
    )

    companion object {
        fun Flag.asBoolean(): Boolean {
            return getBoolean(holder = this.valueHolder)
        }

        fun Flag.asIntRange(): IntegerRangeValue {
            return getIntegerRange(holder = this.valueHolder)
        }

        fun <T : Variant> Flag.asVariant(): T {
            return getVariant(holder = this.valueHolder)
        }

        fun Flag.asInt(): Int {
            return getInt(holder = this.valueHolder)
        }

        fun Flag.asString(): String {
            return getString(holder = this.valueHolder)
        }

        fun getInt(holder: ValueHolder): Int {
            if (holder is IntegerValue) {
                return holder.value
            }
            if (holder is IntegerRangeValue) {
                return holder.getCurrentValue()
            }

            throw IllegalStateException("$holder")
        }

        fun getBoolean(holder: ValueHolder): Boolean {
            if (holder is BooleanValue) {
                return holder.value
            }

            throw IllegalStateException("$holder")
        }

        fun getString(holder: ValueHolder): String {
            if (holder is StringValue) {
                return holder.value
            }
            if (holder is ListValue<*>) {
                return holder.value
            }

            throw IllegalStateException("$holder")
        }

        fun <T : Variant> getVariant(holder: ValueHolder): T {
            if (holder is ListValue<*>) {
                return holder.getVariant()
            }

            throw IllegalStateException("$holder")
        }

        fun getIntegerRange(holder: ValueHolder): IntegerRangeValue {
            if (holder is IntegerRangeValue) {
                return holder
            }

            throw IllegalStateException("$holder")
        }

        fun findByKey(prefKey: String): Flag? {
            return EnumSet.allOf(Flag::class.java).firstOrNull {
                it.preferenceKey == prefKey
            }
        }
    }
}